import {BrowserRouter , Route , Routes } from 'react-router-dom'
import Dashboard from './pages/Dashboard/Dashboard';
import Layout from './components/Layout/Layout';
import Calendar from './pages/Machine/Calendar';
import Board from './pages/User/Board';

function App() {
  return (
    <div id='dashboard'>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Layout/>}>
              <Route path='dashboard' element={<Dashboard/>}/>
              <Route path='calendar' element={<Calendar/>}/>
              <Route path='board' element={<Board/>}/>
              <Route path='dashboard/board' element={<Board/>}/>
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
