import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import Order from "../../components/User/Order";
import css from "./Calendar.module.css";
import {RiShieldUserFill} from "react-icons/ri";

function Calendar() {
  const [socket, setSocket] = useState(null);
  const [datas, setDatas] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [userPresent, setUserPresent] = useState(false);
  const [isLoading, setIsLoading] = useState(true); 
  const location = useLocation();

  useEffect(() => {
    const newSocket = new WebSocket("ws://localhost:7788");

    const searchParams = new URLSearchParams(location.search);
    const sn = searchParams.get("sn");
    console.log("sn at all ....:", sn);

    newSocket.onopen = () => {
      console.log("WebSocket connection established for All Machine.");
      setSocket(newSocket);
      const formData = {
        cmd: "getUsers",
        sn: sn,
      };
      const dataToSend = JSON.stringify(formData);
      if (newSocket.readyState === newSocket.OPEN) {
        console.log("allMachines Command sent..");
        newSocket.send(dataToSend);
      } else {
        console.log("Dashboard WebSocket connection is not available.");
      }
    };

    newSocket.onerror = (error) => {
      console.error("WebSocket error:", error);
     
    };

    newSocket.onclose = (event) => {
      setIsLoading(false);
      console.log("WebSocket connection closed:", event.code, event.reason);
    };

    newSocket.onmessage = (event) => {
      const newData = JSON.parse(event.data);
      console.log("data from server in all user after click", newData);
      
      setIsLoading(false);
      // const updatedDatas = Object.values(newData);
      setDatas(newData);
      
      console.log("datas after setting in user sn click:", datas);
    };   

    const handleBeforeUnload = () => {
      if (socket) {
        socket.close();
      }
    };
    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      if (socket) {
        socket.close();
      }
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, [location.search]);

  const handleSearch = (e) => {
    const val = Number(e.target.value);
  
    if (e.target.value.length > 0) {
      const filteredUsers = datas.filter((item) => item.enrollid === val);
      console.log("in search filter", filteredUsers);
      setSearchQuery(filteredUsers);
      setUserPresent(filteredUsers.length > 0);
    } else {
      setSearchQuery([]);
      setUserPresent(false);
    }
  };

  return (
    <div className={css.container}>
      <div className={css.dashboard}>
        <div className={`${css.dashboardHead} theme-container`}>
          <div className={css.head}>    
            <span>User On Machines</span>
            <div> 
              <input
                className={css.inp}
                type="text"
                placeholder="search enroll Id"
                onChange={handleSearch}
              />
            </div>
          </div>
          <div className={css.cards}>
            <div className={css.inn}>
              {isLoading ? ( 
                <div className={css.loader}></div> 
              ) : datas.length > 0 ? (
                <table className={css.tableLayout}>
                  <thead>
                    <tr>
                    <th className= {css.headt}>Enroll Id</th>
                    <th className={css.headt}>name</th>
    
                    </tr>
                  </thead>  
                  <tbody>
                    {datas.map((user, index) => ( 
                      <tr key={index}>
                        <td>{user.enrollid}</td>
                        <td>{user.name}</td>
                                
                      </tr>
                    )) }
                  </tbody>
                </table>
              ) : (
                <div className={css.pleaseselect}>Please Select Machine On active Machine Table</div>
              )}
            </div>
            {userPresent && <div className={css.present}><span className={css.spnTi}><RiShieldUserFill/></span>   User present in machine</div>}
          </div> 
        </div>
      </div>
      <Order/>
    </div>
  );
}

export default Calendar;
