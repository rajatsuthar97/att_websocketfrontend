// import Order from "../../components/Orders/Order";
import css from "./board.module.css";
import { BsFillHouseAddFill } from "react-icons/bs";
import { useState, useEffect } from "react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import BlockDemo from "../../components/AddAttendance/BlockDemo";

const client = new W3CWebSocket("ws://127.0.0.1:7788");

function Board() {
  const [enrollid, setEnrollid] = useState("");
  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [mobile, setMobile] = useState("");
  const [isDataSent, setIsDataSent] = useState(false);

  const handleEnrollidChange = (event) => {
    setEnrollid(event.target.value);
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleTypeChange = (event) => {
    setType(event.target.value);
  };

  const handleMobileChange = (event) => {
    setMobile(event.target.value);
  };

  const submitHandle = () => {
    if (!enrollid || !name || !type || !mobile) {
      toast.error("Please fill in all required fields");
      return;
    }

    const formData = {
      cmd: "managerAddUser",
      enrollid: enrollid,
      name: name,
      type: type,
      mobileNumber: mobile,
    };

    const dataToSend = JSON.stringify(formData);

    if (client.readyState === client.OPEN) {
      console.log("Sending data through add user WebSocket...");
      client.send(dataToSend);
    } else {
      console.log("WebSocket connection is not available");
      toast.error("WebSocket connection is not available");
    }
  };

  useEffect(() => {
    client.onopen = () => {
      console.log("WebSocket connection established for add user");
    };

    client.onmessage = (message) => {
      const dataFromServer = JSON.parse(message.data);

      if (dataFromServer.result) {
        console.log("Data sent successfully!", dataFromServer);
        setIsDataSent(true);
        toast.success("Data added successfully!");
      } else {
        console.log("Failed to send data!", dataFromServer);
        toast.error("Failed to send data!");
      }
      setEnrollid("");
      setName("");
      setType("");
      setMobile("");
    };

    client.onerror = (error) => {
      console.error("WebSocket error:", error);
    };

    client.onclose = (event) => {
      console.log("WebSocket connection closed:", event.code, event.reason);
    };

    return () => {};
  }, [submitHandle]);

  return (
    <div className={css.container}>
      {/* ---------------------left card-------------------------------- */}
      <div className={css.dashboard}>
        <div className={`${css.dashboardHead} theme-container`}>
          <div className={css.head}>
            <span>Add Users</span>
            <div className={css.durationButton}>
              <span>
                <BsFillHouseAddFill size={17} />
              </span>
            </div>
          </div>
          <div className={css.cards}>
            <div className={css.conDiv}>
              <div className={css.form}>
                <div className={`${css.inputContainer} ${css.ic1}`}>
                  <input
                    id="userId"
                    className={css.input}
                    type="text"
                    placeholder=" "
                    value={enrollid}
                    onChange={handleEnrollidChange}
                    required
                  />
                  <div className={css.cut}></div>
                  <label htmlFor="userId" className={css.placeholder}>
                    User Id
                  </label>
                </div>
                <div className={`${css.inputContainer} ${css.ic2}`}>
                  <input
                    id="name"
                    className={css.input}
                    type="text"
                    placeholder=" "
                    value={name}
                    onChange={handleNameChange}
                    required
                  />
                  <div className={css.cut}></div>
                  <label htmlFor="name" className={css.placeholder}>
                    Name
                  </label>
                </div>
                <div className={`${css.inputContainer} ${css.ic2}`}>
                  <select
                    id="type"
                    className={css.input}
                    value={type}
                    onChange={handleTypeChange}
                    required
                  >
                    <option value="">Select Type</option>
                    <option value="student">Student</option>
                    <option value="employee">Employee</option>
                  </select>
                  <div className={`${css.cut} ${css.cutShort}`}></div>
                  {/* <label htmlFor="type" className={css.placeholder}>
                    Type
                  </label> */}
                </div>
                <div className={`${css.inputContainer} ${css.ic2}`}>
                  <input
                    id="mobile"
                    className={css.input}
                    type="text"
                    placeholder=" "
                    value={mobile}
                    onChange={handleMobileChange}
                    required
                  />
                  <div className={`${css.cut} ${css.cutShort}`}></div>
                  <label htmlFor="mobile" className={css.placeholder}>
                    Mobile
                  </label>
                </div>
                <button
                  onClick={submitHandle}
                  type="text"
                  className={css.submit}
                >
                  Add User
                </button>
              </div>
            </div>
            <div className={css.innimg}>
              <img src="../dot.png" alt="dot" />
            </div>
          </div>
        </div>
        <ToastContainer />
      </div>

      {/* ------------------------right card----------------------------- */}

      <BlockDemo />
    </div>
  );
}

export default Board;
