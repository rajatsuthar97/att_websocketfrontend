import React from "react";
import ActiveMachine from "../../components/ActiveMachine/ActiveMachine";

function Dashboard() {
  return (
    <>
      <ActiveMachine />
    </>
  );
}

export default Dashboard;
