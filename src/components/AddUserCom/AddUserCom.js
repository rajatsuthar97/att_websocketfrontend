import { useState, useEffect } from "react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import css from "./AddUserCom.module.css";
import { w3cwebsocket as W3CWebSocket } from "websocket";

const client = new W3CWebSocket("ws://127.0.0.1:7788");

function AddUserCom() {
  const [snNumber, setSnNumber] = useState("");
  const [enrollid, setEnrollid] = useState("");
  const [backup, setBackUp] = useState("");
  const [isDataSent, setIsDataSent] = useState(false);

  const handleSnNumberChange = (event) => {
    setSnNumber(event.target.value);
  };

  const handleEnrollidChange = (event) => {
    setEnrollid(Number(event.target.value));
  };

  const handleBackUp = (event) => {
    setBackUp(Number(event.target.value));
  };

  useEffect(() => {
    client.onopen = () => {
      console.log("WebSocket connection established for add finger");
    };

    client.onmessage = (message) => {
      const dataFromServer = JSON.parse(message.data);

      if (dataFromServer.result) {
        console.log("Data finger sent successfully!");
        setIsDataSent(true);
        toast.success("Data added successfully!");
      } else {
        console.log("Failed to send data!", dataFromServer);
        toast.error("Failed to send data!");
      }
    };

    client.onerror = (error) => {
      console.error("WebSocket error:", error);
    };

    client.onclose = (event) => {
      console.log("WebSocket connection closed:", event.code, event.reason);
    };

    return () => {};
  }, []);

  const submitHandle = () => {
    if (!snNumber || !enrollid || !backup) {
     toast.error("Please fill in all required fields")
      return;
    }

    const formData = {
      sn: snNumber,
      enrollid: enrollid,
      backupnum: backup,
      cmd: "addUserRecord",
    };

    const dataToSend = JSON.stringify(formData);

    if (client.readyState === client.OPEN) {
      console.log("Sending data through finger WebSocket...");
      client.send(dataToSend);
    } else {
      console.log("WebSocket connection is not available.");
      toast.error("WebSocket connection is not available.");
    }
  };

  return (
    <div className={`${css.container}`}>
      <div className={css.demoDiv}>
        <div className={css.conDiv}>
          <div className={css.form}>
            <div className={`${css.inputContainer} ${css.ic1}`}>
              <input
                id="snNumber"
                className={css.input}
                type="text"
                placeholder=" "
                value={snNumber}
                onChange={handleSnNumberChange}
                required
              />
              <div className={css.cut}></div>
              <label htmlFor="snNumber" className={css.placeholder}>
                SN Number
              </label>
            </div>
            <div className={`${css.inputContainer} ${css.ic1}`}>
              <input
                id="enrolld"
                className={css.input}
                type="number"
                placeholder=" "
                value={enrollid}
                onChange={handleEnrollidChange}
                required
              />
              <div className={css.cut}></div>
              <label htmlFor="enrollid" className={css.placeholder}>
                enroll Id
              </label>
            </div>
            <div className={`${css.inputContainer} ${css.ic2}`}>
              <input
                id="backup"
                className={css.input}
                type="number"
                placeholder=" "
                value={backup}
                onChange={handleBackUp}
                required
              />
              <div className={css.cut}></div>
              <label htmlFor="fpMachine" className={css.placeholder}>
                backup Number
              </label>
            </div>
           <button type="submit" className={css.submit} onClick={submitHandle}>
            Submit
           </button>
          </div>
        </div>
      </div>

      <ToastContainer />
    </div>
  );
}

export default AddUserCom;
