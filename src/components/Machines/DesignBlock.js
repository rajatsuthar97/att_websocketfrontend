import React, { useEffect, useState } from "react";
import css from "./DesignBlock.module.css";
import { NavLink } from "react-router-dom";
import { IoNotificationsSharp } from "react-icons/io5";
import { FcCheckmark } from "react-icons/fc";
import { toast, ToastContainer } from "react-toastify";

function DesignBlock() {
  const [socket, setSocket] = useState(null);
  const [datas, setDatas] = useState([]);
  const [showThead, setShowThead] = useState(true);

  useEffect(() => {
    const newSocket = new WebSocket("ws://localhost:7788");

    newSocket.onopen = () => {
      console.log("WebSocket connection established for dashboard.");
      setSocket(newSocket);

      const formData = {
        cmd: "getNewUser",
      };
      const dataToSend = JSON.stringify(formData);
      if (newSocket.readyState === newSocket.OPEN) {
        console.log("getNewUser Command sent..");
        newSocket.send(dataToSend);
      } else {
        console.log("Dashboard WebSocket connection is not available.");
      }
    };

    newSocket.onerror = (error) => {
      console.error("WebSocket error:", error);
    };

    newSocket.onclose = (event) => {
      console.log("WebSocket connection closed:", event.code, event.reason);
    };

    newSocket.onmessage = (event) => {
      const newData = JSON.parse(event.data);
      const updatedDatas = Object.values(newData).map((item) => ({
        ...item,
        verified: false,
      }));
      setDatas(updatedDatas);
    };

    const handleBeforeUnload = () => {
      if (socket) {
        socket.close();
      }
    };
    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      if (socket) {
        socket.close();
      }
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, []);

  const sendStatusToServer = (enrollid, snNumber, _id) => {
    if (socket) {
      const formData = {
        cmd: "verifyUser",
        enrollid: enrollid,
        snNumber: snNumber,
        _id: _id,
      };
      const dataToSend = JSON.stringify(formData);
      socket.send(dataToSend);
      console.log(";;;;;", dataToSend);

      socket.onmessage = (event) => {
        const newData = JSON.parse(event.data);
        console.log("verrrr", newData);
        if (newData.result) {
          const updatedDatas = datas.map((item) =>
            item._id === _id ? { ...item, verified: true } : item
          );

          setDatas(updatedDatas);
          
        } else {
          // console.log("no no")
          toast.error("please go to user page & add user");
        }
      };
    }
  };

  const handleVerificationClick = (item) => {
    sendStatusToServer(item.enrollid, item.sn, item._id);
  };

  useEffect(() => {
    setShowThead(!document.querySelector(`.${css.colspn}`));
  }, [datas]);

  return (
    <div className={`${css.container} theme-container`}>
      <div className={css.btnDiv}>
        <NavLink to="board" className={css.item} title={"Add New User"}>
          <button className={css.adduser}>Add user</button>
        </NavLink>
      </div>
      <div className={css.imgg}>
        <img src="./bio21.png" alt="bio1" />
      </div>
      <div className={css.icondiv}>
        <IoNotificationsSharp fontSize={11} />
        <span className={css.headtext}>New User</span>
      </div>
      <div className={css.card}>
        <table>
          {showThead && datas.length > 0 && (
            <thead>
              <tr className={css.thdd}>
                <th>SN Number</th>
                <th>Enroll Id</th>
                <th>Status</th>
              </tr>
            </thead>
          )}
          <tbody className={css.tbo}>
            {datas.length === 0 ? (
              <tr>
                <td className={`${css.colspn} ${css.tdColspn}`} colSpan="3">
                  New user is not present at the moment
                </td>
              </tr>
            ) : (
              datas.map((item) => (
                <tr key={item.sn}>
                  <td>{item.sn}</td>
                  <td>{item.enrollid}</td>
                  <td
                    onClick={() => handleVerificationClick(item)}
                    style={{ cursor: "pointer" }}
                  >
                    {item.verified ? <FcCheckmark size={12} /> : item.status}
                  </td>
                </tr>
              ))
            )}
          </tbody>
        </table>
      </div>
      <ToastContainer />
    </div>
  );
}

export default DesignBlock;
