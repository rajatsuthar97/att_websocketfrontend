import { useState, useEffect } from "react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import css from "./Order.module.css";
import { w3cwebsocket as W3CWebSocket } from "websocket";



const client = new W3CWebSocket("ws://127.0.0.1:7788");

function Order() {
  const [snNumber, setSnNumber] = useState("");
  const [fpMachine, setFpMachine] = useState("");
  const [venue, setVenue] = useState("");
  const [isDataSent, setIsDataSent] = useState(false);
  
 

  const handleSnNumberChange = (event) => {
    setSnNumber(event.target.value);
  };

  const handleFpMachineChange = (event) => {
    setFpMachine(event.target.value);
  };

  const handleVenueChange = (event) => {
    const uppercaseVenue = event.target.value.toUpperCase();
    setVenue(uppercaseVenue);
  };

  useEffect(() => {
    client.onopen = () => {
      console.log("WebSocket connection established for add machine");
    };

    client.onmessage = (message) => {
      const dataFromServer = JSON.parse(message.data);

      if (dataFromServer.result) {
        console.log("Data sent successfully!");
        setIsDataSent(true);
        toast.success("Data added successfully!");
      } else {
        console.log("Failed to send data!", dataFromServer);
        toast.error("Failed to send data!");
      }
      setSnNumber("");
      setFpMachine("");
      setVenue("");
    };

    client.onerror = (error) => {
      console.error("WebSocket error:", error);
    };

    client.onclose = (event) => {
      console.log("WebSocket connection closed:", event.code, event.reason);
    };

    return () => {};
  }, []);

  const submitHandle = () => {
    if (!snNumber || !fpMachine || !venue) {
      toast.error("Please fill in all required fields.");
      return;
    }

    const formData = {
      sn: snNumber,
      type: fpMachine,
      cmd: "reg",
      venue: venue,
    };

    const dataToSend = JSON.stringify(formData);

    if (client.readyState === client.OPEN) {
      console.log("Sending data through WebSocket...");
      client.send(dataToSend);
    } else {
      console.log("WebSocket connection is not available.");
      toast.error("WebSocket connection is not available.");
    }
  };

  return (
    <div className={`${css.container} theme-container`}>
      <div className={css.demoDiv}>
        <div className={css.head}>
          <span className={css.heading1}>Add Machine</span>
        </div>

        <div className={css.conDiv}>
          <div className={css.form}>
            <div className={`${css.inputContainer} ${css.ic1}`}>
              <input
                id="snNumber"
                className={css.input}
                type="text"
                placeholder=" "
                value={snNumber}
                onChange={handleSnNumberChange}
                required
              />
              <div className={css.cut}></div>
              <label htmlFor="snNumber" className={css.placeholder}>
                SN Number
              </label>
            </div>
            <div className={`${css.inputContainer} ${css.ic2}`}>
              <input
                id="fpMachine"
                className={css.input}
                type="text"
                placeholder=" "
                value={fpMachine}
                onChange={handleFpMachineChange}
                required
              />
              <div className={css.cut}></div>
              <label htmlFor="fpMachine" className={css.placeholder}>
                Type
              </label>
            </div>
            <div className={`${css.inputContainer} ${css.ic2}`}>
              <input
                id="venue"
                className={css.input}
                type="text"
                placeholder=" "
                value={venue}
                onChange={handleVenueChange}
                required
              />
              <div className={`${css.cut} ${css.cutShort}`}></div>
              <label htmlFor="venue" className={css.placeholder}>
                Venue
              </label>
            </div>
            <button onClick={submitHandle} type="text" className={css.submit}>
              Submit
            </button>
          </div>
        </div>
       
      </div>

      

      <ToastContainer />
    </div>
  );
}

export default Order;
