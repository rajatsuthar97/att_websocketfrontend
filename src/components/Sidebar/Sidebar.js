import css from "./Sidebar.module.css";
import { MdSpaceDashboard } from "react-icons/md";
import { AiFillCalendar, AiOutlineTable } from "react-icons/ai";
import { FaTasks } from "react-icons/fa";
import { FiLogOut } from "react-icons/fi";

import { NavLink } from "react-router-dom";

function Sidebar() {
  return (
    <div className={css.container}>
      {/* <img src="./logo.png" alt="logo" className={css.logo} /> */}

      <div className={css.menu}>
        <NavLink to="dashboard" className={css.item} title={"Dashboard"}>
          <MdSpaceDashboard size={30} />
        </NavLink>

        <NavLink to="calendar" className={css.item} title={"Machine"}>
          <AiFillCalendar size={30} />
        </NavLink>

        <NavLink to="board" className={css.item} title={"User"}>
          <FaTasks size={30} />
        </NavLink>

        <NavLink to="users" className={css.item} title={"users"}>
          <AiOutlineTable size={30} />
        </NavLink>
      </div>
      <div className={css.logoutDiv}>
        <FiLogOut />
        <p>logout</p>
      </div>
    </div>
  );
}

export default Sidebar;
