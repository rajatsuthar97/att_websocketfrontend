import { useState, useEffect } from "react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import css from "./BlockDemo.module.css";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Modal from 'react-bootstrap/Modal';
import AddUserCom from "../AddUserCom/AddUserCom";

const client = new W3CWebSocket("ws://127.0.0.1:7788");

function BlockDemo() {
  const [snNumber, setSnNumber] = useState("");
  const [date, setDate] = useState(new Date());
  const [enrollid, setEnrollid] = useState("");
  const [isDataSent, setIsDataSent] = useState(false);
  const [show, setShow] = useState(false);

  const handleSnNumberChange = (event) => {
    setSnNumber(event.target.value);
  };

  const handleEnrollidChange = (event) => {
    setEnrollid(event.target.value);
  };

  const handleDateChange = (selectedDate) => {
    setDate(selectedDate);
  };

  useEffect(() => {
    client.onopen = () => {
      console.log("WebSocket connection established.");
    };

    client.onmessage = (message) => {
      const dataFromServer = JSON.parse(message.data);

      if (dataFromServer.result) {
        console.log("Data sent successfully!");
        setIsDataSent(true);
        toast.success("Data added successfully!");
      } else {
        console.log("Failed to send data!", dataFromServer);
        toast.error("Failed to send data!");
      }
      setSnNumber("");
      setDate(new Date());
      setEnrollid("");
    };

    client.onerror = (error) => {
      // toast.error("WebSocket Connection Closed");
      console.error("WebSocket error:", error);
    };

    client.onclose = (event) => {
      console.log("WebSocket connection closed:", event.code, event.reason);
    };

    return () => {};
  }, []);

  const submitHandle = () => {
    if (!snNumber || !date || !enrollid) {
      toast.error("Please fill in all required fields.");
      return;
    }

    const formData = {
      cmd: "sendlog",
      sn: snNumber,
      record: [
        {
          enrollid: enrollid,
          time: date.toISOString()
        },
      ],
    };

    const dataToSend = JSON.stringify(formData);

    if (client.readyState === client.OPEN) {
      console.log("Sending data through sendlog WebSocket...", dataToSend);
      client.send(dataToSend);

      client.onmessage = (message) => {
        const dataFromServer = JSON.parse(message.data);

        if (dataFromServer.result) {
          console.log("Data sent successfully!");
          // setIsDataSent(true);
          toast.success("Data added successfully!");
        } else {
          console.log("Failed to send data!", dataFromServer);
          toast.error("Failed to send data!");
        }
        setSnNumber("");
        setDate(new Date());
        setEnrollid("");
      };
    } else {
      console.log("WebSocket connection is not available.");
      toast.error("WebSocket connection is not available.");
    }
  };

  return (
    <div className={`${css.container} theme-container`}>
      <div className={css.demoDiv}>
        <div className={css.head}>
          <span className={css.heading1}>Add Attendance</span>
        </div>

        <div className={css.conDiv}>
          <div className={css.form}>
            <div className={`${css.inputContainer} ${css.ic1}`}>
              <input
                id="userId"
                className={css.input}
                type="text"
                placeholder=" "
                value={enrollid}
                onChange={handleEnrollidChange}
                required
              />
              <div className={css.cut}></div>
              <label htmlFor="userId" className={css.placeholder}>
                User Id
              </label>
            </div>
            <div className={`${css.inputContainer} ${css.ic1}`}>
              <input
                id="snNumber"
                className={css.input}
                type="text"
                placeholder=" "
                value={snNumber}
                onChange={handleSnNumberChange}
                required
              />
              <div className={css.cut}></div>
              <label htmlFor="snNumber" className={css.placeholder}>
                SN Number
              </label>
            </div>

            <div className={`${css.inputContainer1} ${css.ic2}`}>
              <DatePicker
                id="time"
                className={css.input1}
                selected={date}
                onChange={handleDateChange}
                dateFormat="dd/MM/yyyy"
                required
              />
              <div className={`${css.cut1} ${css.cutShort1}`}></div>
              {/* <label htmlFor="time" className={css.placeholder1}>
                Date
              </label> */}
            </div>
            <button onClick={submitHandle} type="text" className={css.submit}>
              Submit
            </button>
          </div>
          
        </div>
        <p className={css.othermachine}>
          add user's fingerprint on other machines ?{" "}
          <span className={css.innerSpn} onClick={() => setShow(true)}>
            Click here
          </span>
        </p>

        {show && (
        <Modal
          show={show}
          onHide={() => setShow(false)}
          dialogClassName={`${css.modalFullscreen} modal-90w`}
          aria-labelledby="example-custom-modal-styling-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="example-custom-modal-styling-title">
              Add User fingerprint
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <AddUserCom/>

          </Modal.Body>
        </Modal>
      )}
      </div>
      <ToastContainer />
    </div>
  );
}

export default BlockDemo;
