import React, { useEffect, useState } from "react";
import css from '../ActiveMachine/ActiveMachine.module.css';
import { FcCheckmark } from "react-icons/fc";
import { BsFillCircleFill } from "react-icons/bs";
import moment from "moment";
import DesignBlock from "../Machines/DesignBlock";
import { useNavigate } from 'react-router-dom';

function ActiveMachine() {
    const [socket, setSocket] = useState(null);
    const [datas, setDatas] = useState([]);
    const [showThead, setShowThead] = useState(true);
    const [editedVenue, setEditedVenue] = useState({ id: null, value: "" });
    const navigate = useNavigate();
    
  
    useEffect(() => {
   
  
      const newSocket = new WebSocket("ws://localhost:7788");
  
      newSocket.onopen = () => {
        console.log("WebSocket connection established for dashboard.");
        setSocket(newSocket);
  
        const formData = {
          cmd: "activeConn",
        };
        const dataToSend = JSON.stringify(formData);
        if (newSocket.readyState === newSocket.OPEN) {
          console.log("activeConn Command sent..");
          newSocket.send(dataToSend);
        } else {
          console.log("Dashboard WebSocket connection is not available.");
        }
      };
  
      newSocket.onerror = (error) => {
        console.error("WebSocket error:", error);
      };
  
      newSocket.onclose = (event) => {
        console.log("WebSocket connection closed:", event.code, event.reason);
      };
  
      newSocket.onmessage = (event) => {
        const newData = JSON.parse(event.data);
  
        console.log("data from server", newData);
        const updatedDatas = Object.values(newData);
        setDatas(updatedDatas);
        console.log("datas after setting in dash:", updatedDatas);
      };
  
      const handleBeforeUnload = () => {
        if (socket) {
          socket.close();
        }
      };
    //   window.addEventListener("beforeunload", handleBeforeUnload);
  
      return () => {
        if (socket) {
          socket.close();
        }
        // window.removeEventListener("beforeunload", handleBeforeUnload);
      };
    }, []);
  
    useEffect(() => {
      setShowThead(!document.querySelector(`.${css.colspn}`));
    }, [datas]);
  
    const handleVenueEdit = (item) => {
      setEditedVenue({ id: item.sn, value: item.venue });
    };
  
    const updateVenue = (item) => {
      const updatedDatas = datas.map((data) => {
        if (data.sn === item.sn) {
          return {
            ...data,
            venue: editedVenue.value,
          };
        }
        return data;
      });
  
      setDatas(updatedDatas);
  
      const formData = {
        cmd: "verifyMachine",
        sn: item.sn,
        venue: editedVenue.value,
      };
      const dataToSend = JSON.stringify(formData);
      if (socket && socket.readyState === socket.OPEN) {
        console.log("updateVenue Command sent..");
        socket.send(dataToSend);
      } else {
        console.log("WebSocket connection is not available");
      }
  
      setEditedVenue({ id: null, value: "" });
    };
  
    const handleVerificationClick = (item) => {
      const updatedDatas = datas.map((data) => {
        if (data.sn === item.sn) {
          return {
            ...data,
            status: "verifying",
          };
        }
        return data;
      });
  
      setDatas(updatedDatas);
  
      const startAt = moment().format("YYYY-MM-DD");
      const currentDate = moment().format("YYYY-MM-DD");
  
      const formData = {
        cmd: "verifyMachine",
        sn: item.sn,
        venueCode: "CP",
        startAt: startAt,
        currentDate: currentDate,
      };
      const dataToSend = JSON.stringify(formData);
      if (socket && socket.readyState === socket.OPEN) {
        socket.send(dataToSend);
        console.log("verifyMachine Command sent..at at",dataToSend);
      } else {
        console.log("WebSocket connection is not available");
      }
  
      socket.onmessage = (event) => {
        const newData = JSON.parse(event.data);
        console.log("data from server verifymachine time", newData);
        if (newData.result === false && newData.reason==="Machine is already verified and attached to a Venue. Please detach it, to make it unverified") {
          const response = "verified";
          handleVerificationResponse(response);
        }
        console.log("succ", newData.result);
      };  
  
      const handleVerificationResponse = (response) => {
        const updatedDatas = datas.map((data) => {
          if (data.sn === item.sn) {
            return {
              ...data,
              status: response ? "verified" : "unverified",
            };
          }
          return data;
        });
  
        setDatas(updatedDatas);
      };
  
      setTimeout(() => {
        handleVerificationResponse(true); 
      }, 2000);
    };
  
    const navigateToAllUser = (sn) => {
        navigate(`/calendar?sn=${sn}`);
        console.log("clicked at")
      };
  
    return (
      <div className={css.container}>
        <div className={css.dashboard}>
          <div className={`${css.dashboardHead} theme-container`}>
            <div className={css.head}>
              <span>Active Machines</span>
              <div className={css.durationButton}>
                {datas.some((item) => item.status === "verified") ? (
                  <BsFillCircleFill size={9} style={{ color: "darkgreen" }} />
                ) : (
                  <BsFillCircleFill
                    size={9}
                    style={{ color: "darkgreen", padding: "0", margin: "0" }}
                  />
                )}
              </div>
            </div>
            <div className={css.cards}>
              <table>
                {showThead && (
                  <thead>
                    <tr className={css.thdd}>
                      <th>Machine</th>
                      <th>Venue</th>
                      <th>Machine Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                )}
                <tbody className={css.tbo}>
                  {datas.length === 0 ? (
                    <tr>
                      <td className={`${css.colspn} ${css.tdColspn}`} colSpan="4">
                        Sorry ! No machine is active
                      </td>
                    </tr>
                  ) : (
                    datas.map((item) => (
                      <tr key={item.sn}>
                        <td className={css.snrow}  onClick={() => navigateToAllUser(item.sn)}>{item.sn}</td>
                        <td>
                          {editedVenue.id === item.sn ? (
                            <input
                              className={css.inputVenue}
                              type="text"
                              value={editedVenue.value}
                              onChange={(e) =>
                                setEditedVenue({
                                  id: editedVenue.id,
                                  value: e.target.value,
                                })
                              }
                            />
                          ) : (
                            item.venue
                          )}
                        </td>
                        <td>
                          {item.status === "unverified" ? (
                            <span
                              onClick={() => handleVerificationClick(item)}
                              style={{ cursor: "pointer" }}
                            >
                              unverified
                            </span>
                          ) : (
                            <FcCheckmark size={12} />
                          )}
                        </td>
                        <td>
                          {editedVenue.id === item.sn ? (
                            <button
                              className={css.btn1}
                              onClick={() => updateVenue(item)}
                            >
                              Save
                            </button>
                          ) : (
                            <button
                              className={css.btn1}
                              onClick={() => handleVenueEdit(item)}
                            >
                              Edit
                            </button>
                          )}
                        </td>
                      </tr>
                    ))
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <DesignBlock />
      </div>
    );
  
}

export default ActiveMachine